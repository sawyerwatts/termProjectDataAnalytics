# Term Project CS410 Data Analytics

## Team Name: Space Race
Members:  Sawyer Watts, Dan Scott, and Medina Lamkin

## Project Goal
The goal of this project is to compare the frequency in which the US and USSR used ExtraVehicular Activity (EVA) in relation to the number of newspaper articles written about the Cold War from 1965 through 1990.

## Project Summary
*  This project will produce a diagram showing the relationship between the US and USSR EVA frequency as a part of the Space Race during the Cold War and the number of keywords and keyphrases that were used in newspaper articles. Through our data visualization, we hope to illustrate the extent to which these two nations competed against each during the Space Race as relations between them changed. We plan to develop this diagram using Plotly, a Python module which we have been familiarizing ourselves with. First, we will need to obtain data about the occurrences of Cold War related keywords in newspaper articles; this data and the EVA data will then be stored in a Google Cloud Database.
*  We will then use Plotly to build the visualization. This graph will have an X axis of Year and there will be two plots overlaid onto the graph. The first Y axis graph will plot the US and USSR EVAs per year. The second Y axis will graph the number of significant events that occurred during that year. This will attempt to correlate the amount of EVAs that each country deployed with the amount of termol the two countries were experiencing during the Cold War. During analysis, specific dates may be looked at to see if they caused a spike in a country’s EVA usage to try to assert superiority to attempt to save face.

## Tasks 
We have nine broad tasks to accomplish during this project as specified in this spreadsheet (https://docs.google.com/spreadsheets/d/16gmEibWSQJ5-YsL8adYShJbkpq3L3ZQtV-WXIN1nVKw/edit?usp=sharing).

## Roles
### Sawyer Watts
*  Team Coordinator
*  Primary Plotly researcher/user 

### Dan Scott
*  Develop Cold War timeline and find related data source
*  Help with the building of the data visualization diagram 
*  Analysis of diagram and evaluation of work done

### Medina Lamkin
*  Build databases and data cleaning as necessary
*  Help with the building of the data visualization diagram 


## Codebase Information
CSVs.tgz contains the CSVs used in main.py; these are not in a directory for ease of loading into main.py

space\_race\_presentation.tgz contains the source used in the demo of space\_race\_tool\_presentation.pdf

