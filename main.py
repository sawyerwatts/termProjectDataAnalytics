import pandas
import plotly.graph_objects
import plotly.subplots

# NOTE: I rolled every instance in a month to the first of that month just to
# make it easier to analyze.

###############################################################################
# Functions
###########
def scrubDates(row):
    return row["Date"].replace(day=1)


###############################################################################
# Main
######

###########################################################
# Load/clean the EVA data.
##########################
raw_eva = pandas.read_csv("eva.csv", parse_dates=["Date"])
raw_eva = raw_eva[["Date", "Country"]]
raw_eva.dropna(inplace=True)
raw_eva.drop_duplicates(inplace=True)

# Scrub off the day so it aggregates by month.
raw_eva["Date"] = raw_eva.apply(scrubDates, axis=1)


###########################################################
# Get country specific EVA data.
################################
us_eva = raw_eva[raw_eva["Country"] == "USA"]
us_eva = us_eva["Date"].value_counts()
us_eva.sort_index(inplace=True)

russia_eva = raw_eva[raw_eva["Country"] == "Russia"]
russia_eva = russia_eva["Date"].value_counts()
russia_eva.sort_index(inplace=True)


###########################################################
# Load/clean the events data.
###################################
raw_events = pandas.read_csv("cold_war_timeline.csv", parse_dates=["Date"])
raw_events = raw_events[["Date", "iff US"]]

# Scrub off the day so it aggregates by month.
raw_events["Date"] = raw_events.apply(scrubDates, axis=1)


###########################################################
# Get country specific event data.
##################################
us_events = raw_events[raw_events["iff US"] == "Y"]["Date"].value_counts()
us_events.sort_index(inplace=True)
russia_events = raw_events[raw_events["iff US"] != "Y"]["Date"].value_counts()
russia_events.sort_index(inplace=True)


###########################################################
# Build the plot.
#################

min_date = min(raw_eva["Date"].min(), raw_events["Date"].min())
min_date = max(min_date, pandas.Timestamp(1947, 1, 1))
max_date = max(raw_eva["Date"].max(), raw_events["Date"].max())
max_date = max(max_date, pandas.Timestamp(1992, 1, 1))

"""
This was used before a double y-axis plot was needed; other changes/additions
are noted inline.
>>> fig = plotly.graph_objects.Figure()
"""
fig = plotly.subplots.make_subplots(specs=[[{"secondary_y": True}]])

fig.update_layout(
    title = "<i>US</i> and <i>Russian</i> <b>EVAs</b>",
    # Add timeseries to the X-axis by using a Timestamp range.
    xaxis_range = [min_date, max_date],
    xaxis_title =  "Time",
    yaxis_title = "<b>EVAs</b> per month",
    barmode = "group"
    )

fig.update_yaxes(title_text="<b>Socio-political Events</b> per month", secondary_y=True) # added for make_subplots

fig.add_trace(
    plotly.graph_objects.Bar(
        x = us_eva.index.to_list(),
        y = us_eva.to_list(),
        name = "<i>US</i> <b>EVAs</b> per Month", # Set legend
    ),
    )

fig.add_trace(
    plotly.graph_objects.Bar(
        x = russia_eva.index.to_list(),
        y = russia_eva.to_list(),
        name = "<i>Russian</i> <b>EVAs</b> per Month", # Set legend
    ),
)

fig.add_trace(
    plotly.graph_objects.Scatter(
        x = us_events.index.to_list(),
        y = us_events.to_list(),
        name = "<i>US</i> <b>Socio-political</b> per month", # set legend
        mode = "markers", # do not connect the values with lines.
    ),
    secondary_y = True, # added for make_subplot
)

fig.add_trace(
    plotly.graph_objects.Scatter(
        x = russia_events.index.to_list(),
        y = russia_events.to_list(),
        name = "<i>Russian</i> <b>Socio-political</b> per month", # set legend
        mode = "markers", # do not connect the values with lines.
    ),
    secondary_y = True, # added for make_subplot
)

fig.show()
